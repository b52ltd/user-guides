Руководство по коду для шаблонов
====================

Пример шаблона с описанием основных моментов.

```php
<?php
// Каждый шаблон должен начинаться с открывающего php-тега.
// В самом начале шаблона импортируются неймспейсы для необходимых классов.
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
// Обязательно оставить пустую строку.

// Затем описываются переменные, переданные из контроллера.
// Переменные не могут появляться "из воздуха" поэтому необъявленные переменные должны быть описаны здесь.
/* @var $this yii\base\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $posts app\models\Post[] */
/* @var $contactMessage app\models\ContactMessage */
// Обзятально составить пустую строку.

// Далее можно проводить настойки шаблона, произвонить простейшую логику и т.д.
$this->title = 'Статьи';

// В самом конце объявляются ассеты.
AppAsset::register($this);
?>

<!-- Пустая строка после вводного php-блока -->
<!-- Для foreach, for, if и др. нужно создавать отдельные php-блоки -->
<?php foreach ($posts as $post): ?>
    <!-- Внутри условий и циклов нужно делать отступы как и в основном коде. -->
    <h2><?= Html::encode($post['title']) ?></h2>
    <p><?= Html::encode($post['shortDescription']) ?></p>
<!-- Вместо `}` нужно использовать `endforeach;`, `endfor;`, `endif;` и т.д. -->
<?php endforeach ?>

<!-- Объявление виджетов можно разбивать или не разбивать на несколько строк для улучшения читаемости. -->
<?php $form = ActiveForm::begin([
    'options' => ['id' => 'contact-message-form'],
    'fieldConfig' => ['inputOptions' => ['class' => 'common-input']],
]); ?>
    <!-- Внутри виджетов тоже делаем отступы. -->
    <?= $form->field($contactMessage, 'name')->textInput() ?>
    <?= $form->field($contactMessage, 'email')->textInput() ?>
    <?= $form->field($contactMessage, 'subject')->textInput() ?>
    <?= $form->field($contactMessage, 'body')->textArea(['rows' => 6]) ?>

    <div class="form-actions">
    	<button type="submit">Сохранить</button>
    </div>
<?php ActiveForm::end(); ?>
<!-- В конце шаблона нужно оставить один перенос строки. -->

```

Все ссылки в шаблоне, в том числе на статические файлы, должны быть проставлены через `yii\helpers\Url`.

```php
<a href="<?= Url::to(['users/view', 'id' => $user->id]) ?>">
	<img src="<?= Url::to('@web/images/avatar.png') ?>" alt="Аватар">
</a>
```

Не стоит злоупотреблять `yii\helpers\Html` внутри шаблонов. Он был придумал для того, чтобы не писать HTML в виджетах. В шаблонах предпочтителен HTML, в виджетах — php-код.

```php
// неправильно
<?= Html::a('Ссылка', ['/articles/update', 'id' => $acticle->id]) ?>

// правильно
<a href="<?= Url::to(['articles/view', 'slug' => $acticle->slug]) ?>">Ссылка</a>
```
