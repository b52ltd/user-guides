Руководство по использованию Yii2
=================================

Все ссылки на экшены должны быть заданы через роуты и параметры.

```php
// неправильно
Url::to('/articles/' . $item->slug)

// правильно
Url::to(['articles/view', 'slug' => $item->slug])
```

В роутах предпочтительно использовать относительные пути к модулям, вьюшкам и контроллерам. 

```php
// app\controllers\ArticlesController

// неправильно
Url::to('/users')
Url::to(['articles/update', 'id' => $aricle->id])

// правильно
Url::to(['users/index'])
Url::to(['update', 'id' => $aricle->id])
```

Исключение: ссылка из админки на основной сайт.

```php
// app\modules\admin\controllers\ArticlesController

Url::to(['index']) // -> /admin/articles/index
Url::to(['/articles/index']) // -> /articles/index
```