HTML and CSS style guide
========================

1. Структура файлов
-------------------

Файлы библиотек должны находиться отдельно от сорцов проекта.

Демо-картинки дожны находиться в отдельной папке, чтобы их можно было легко отделить от картинок вёрстки.

2. Родительские селекторы
-------------------------

При использовании препроцессоров не стоит злоупотреблять родительскими селекторами. Большое количетво вложенностей затрудняет чтение и делает невозможным поиск по классу. 

```less
// плохо
.sidebar {
	...

	&-article {
		...
	
		&__header {
			...
		
			&:hover {
				.sidebar-article__title {
					...
				}
			}
		}

		&__title {
			...
		}
	}
}
```

Оптимальный вариант — использовать родительские селекты для элементов и модификаторов

```less
// хорошо
.sidebar {
	...
}

.sidebar-article {
	...
	
	&__header {
		...
	}

	&__title {
		...
	}
	
	&__header:hover &__title {
		...
	}
}

```

3. Хелперы
----------

Если вёрстка делается с помощью Bootstrap, то не стоит забывать про большое количество уже созданных классов-хелперов.

[http://getbootstrap.com/css/#type-alignment]()

```
.text-left
.text-center
.text-right
.text-justify
.text-nowrap

.text-lowercase
.text-uppercase
.text-capitalize
```

[http://getbootstrap.com/css/#helper-classes]()

```
.text-muted
.text-primary
.text-success

.pull-left
.pull-right
.center-block
```

Если вёрстка делается не на Bootstrap, то следует придерживаться такого же именования классов, для того, чтобы другие верстальщики не создавали свои такие же.

4. Основные ошибки в БЭМ 
------------------------

[https://ru.bem.info/faq/]()

Элементы не могут использоваться без блока или находиться за его пределами

```html
<!-- не надо так -->
<div class="breadcrumbs__wrap">
	<div class="breadcrumbs"></div>
</div>

<!-- и так тоже не надо -->
<div class="features-block"></div>
<div class="help-block">
	<div class="features-block__title"></div>
</div>
```

В БЭМ не бывает элементов от элементов `.block__elem1__elem2`.

[https://ru.bem.info/faq/#Почему-в-БЭМ-не-рекомендуется-создавать-элементы-элементов-block__elem1__elem2]()

```html
<!-- плохо -->
<div class="article">
    <div class="article__header">
        <div class="article__header__title"></div>
    </div>
    <div class="article__cotnent"></div>
</div>

<!-- лакшери -->
<div class="article">
    <div class="article__header">
        <div class="article__title"></div>
    </div>
    <div class="article__cotnent"></div>
</div>
```

Не нужно использовать модификаторы для позиционирования блоков внутри других блоков

```html
<!-- плохо -->

<div class="panel">
	<div class="panel__title"></div>
	<button class="btn btn--panel"></button>
</div>

<style>
.btn--panel { margin-top: 10px; }
</style>

<!-- добротно -->

<div class="panel">
	<div class="panel__title"></div>
	<div class="panel__footer">
		<button class="btn"></button>
	</div>
</div>

<style>
.panel__footer { margin-bottom: 10px; }
</style>
```