Руководство по стилю кода
=========================

Данное руководство расширяет и дополняет основной стандарт кодирования [PSR-2].

1. Основные положения
---------------------

- В файлах НЕОБХОДИМО использовать только теги `<?php` и `<?=`.

- Классам НЕОБХОДИМО давать имена в стиле StudlyCaps.

- Константам классов НЕОБХОДИМО давать имена в верхнем регистре с символом подчёркивания в качестве разделителя.

- Методам и переменным НЕОБХОДИМО давать имена в стиле camelCase.

- Файл не должен иметь импортированные неиспользуемые или несуществующие классы.

### 1.1. Пример

```php
<?php
namespace Vendor\Package;

use FooInterface;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

class Foo extends Bar implements FooInterface
{
    public function sampleFunction($a, $b = null)
    {
        if ($a === $b) {
            bar();
        } elseif ($a > $b) {
            $foo->bar($arg1);
        } else {
            BazClass::bar($arg2, $arg3);
        }
    }

    final public static function bar()
    {
        // тело метода
    }
}
```


